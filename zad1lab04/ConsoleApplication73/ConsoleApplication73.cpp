// ConsoleApplication73.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <deque>
#include <clocale>
#include <string>
#include "geom.h"
#include "ConsoleApplication73.h"

using namespace std;

Triangle dodajTrojkat()
{
	Triangle t = *(new Triangle());
	cout << "\n\n\n|----DODAWANIE_TROJKATA_DO_LISTY-----|";
	cout << "\nPodaj wsp�rzedn� x pkt A= ";
	cin >> t.a.x;
	cout << "\nPodaj wsp�rzedn� y pkt A= ";
	cin >> t.a.y;
	cout << "\nPodaj wsp�rzedn� x pkt B= ";
	cin >> t.b.x;
	cout << "\nPodaj wsp�rzedn� y pkt B= ";
	cin >> t.b.y;
	cout << "\nPodaj wsp�rzedn� x pkt C= ";
	cin >> t.c.x;
	cout << "\nPodaj wsp�rzedn� y pkt C= ";
	cin >> t.c.y;
	return t;
}
Rectangle dodajProstokat() {
	Rectangle p = *(new Rectangle());
	cout << "\n\n\n|----DODAWANIE_PROSOTK�TA_DO_LISTY-----|";
	cout << "\nPodaj wsp�rzedn� x pkt A= ";
	cin >> p.a.x;
	cout << "\nPodaj wsp�rzedn� y pkt A= ";
	cin >> p.a.y;
	cout << "\nPodaj wsp�rzedn� x pkt B= ";
	cin >> p.b.x;
	cout << "\nPodaj wsp�rzedn� y pkt B= ";
	cin >> p.b.y;
	cout << "\nPodaj wsp�rzedn� x pkt C= ";
	cin >> p.c.x;
	cout << "\nPodaj wsp�rzedn� y pkt C= ";
	cin >> p.c.y;
	cout << "\nPodaj wsp�rzedn� x pkt D= ";
	cin >> p.d.x;
	cout << "\nPodaj wsp�rzedn� y pkt D= ";
	cin >> p.d.y;
	return p;
}
Rhombus dodajRomb() {
	Rhombus p = *(new Rhombus());
	cout << "\n\n\n|----DODAWANIE_ROMBU_DO_LISTY-----|";
	cout << "\nPodaj wsp�rzedn� x pkt A= ";
	cin >> p.a.x;
	cout << "\nPodaj wsp�rzedn� y pkt A= ";
	cin >> p.a.y;
	cout << "\nPodaj wsp�rzedn� x pkt B= ";
	cin >> p.b.x;
	cout << "\nPodaj wsp�rzedn� y pkt B= ";
	cin >> p.b.y;
	cout << "\nPodaj wsp�rzedn� x pkt C= ";
	cin >> p.c.x;
	cout << "\nPodaj wsp�rzedn� y pkt C= ";
	cin >> p.c.y;
	cout << "\nPodaj wsp�rzedn� x pkt D= ";
	cin >> p.d.x;
	cout << "\nPodaj wsp�rzedn� y pkt D= ";
	cin >> p.d.y;
	return p;
}

Square dodajKwadrat() {
	Square p = *(new Square());
	cout << "\n\n\n|----DODAWANIE_KWADRATU_DO_LISTY-----|";
	cout << "\nPodaj wsp�rzedn� x pkt A= ";
	//cin >> p.Rectangle::a.x;
	cin >> p.a.x;
	cout << "\nPodaj wsp�rzedn� y pkt A= ";
	cin >> p.a.y;
	cout << "\nPodaj wsp�rzedn� x pkt B= ";
	cin >> p.b.x;
	cout << "\nPodaj wsp�rzedn� y pkt B= ";
	cin >> p.b.y;
	cout << "\nPodaj wsp�rzedn� x pkt C= ";
	cin >> p.c.x;
	cout << "\nPodaj wsp�rzedn� y pkt C= ";
	cin >> p.c.y;
	cout << "\nPodaj wsp�rzedn� x pkt D= ";
	cin >> p.d.x;
	cout << "\nPodaj wsp�rzedn� y pkt D= ";
	cin >> p.d.y;
	return p;
}
double SumaObwodow(vector < Polygon *> obiekty)
{
	double suma = 0;

	for (int i = 0; i < obiekty.size(); i++)
	{
		suma += obiekty[i]->perimiter();
	}
	return suma;
}



int main()
{
	setlocale(LC_ALL, "polish");

	int operacja = 0;
	vector < Polygon* > obiekty;
	do {
		operacja = 0;
		cout << "Obiekty:\n";
		for (int i = 0; i < obiekty.size(); i++)
		{
			cout << "numer=" << i << "   ";
			cout << "Typ=" << typeid(*obiekty[i]).name() << "   ";
			cout << "obw�d=" << obiekty[i]->perimiter() << "   ";
			cout << "pole=" << obiekty[i]->area() << "   ";
			cout << "Rozmiar=" << obiekty[i]->size() << " " << endl;
		}

		cout << "suma obwod�w=" << SumaObwodow(obiekty) << endl << endl;
		cout << "Jak� operacj� wykona�?" << endl;
		cout << "1-Dodaj tr�jk�t" << endl;
		cout << "2-Dodaj prostok�t" << endl;
		cout << "3-Dodaj romb" << endl;
		cout << "4-Dodaj kwadrat" << endl;

		cout << "5-Por�wnaj pola" << endl;
		cout << "6-uzupelnij" << endl;
		cin >> operacja;

		switch (operacja)
		{
		case (0):break;
		case(1):obiekty.push_back(&dodajTrojkat());

			break;
		case(2):obiekty.push_back(&dodajProstokat());
			break;
		case(3):obiekty.push_back(&dodajRomb());
			break;
		case(4):obiekty.push_back(&dodajKwadrat());
			break;
		case(5):
		{
			int pierwszy, drugi;
			cout << "kt�ry obiekt chcesz por�wna�?";
			cin >> pierwszy;
			cout << "\nz kt�rym obiektem?";
			cin >> drugi;
			if (obiekty[pierwszy]->hasGreaterArea(obiekty[drugi]))
			{
				cout << "Pierwszy ma wi�ksze pole"<<endl;
			}
			else {
				cout << "Drugi ma wi�ksze pole" << endl;
			}
		}
		break;
		case(6):
			SampleData(obiekty);
			break;
		default: break;

		}


	} while (operacja != 0);


	getchar();
	return 0;
}

void SampleData(std::vector<Polygon *> &obiekty)
{
	auto r = new Rectangle();
	r->a.x = 1;
	r->a.y = 2;
	r->b.x = 3;
	r->b.y = 6;
	r->c.x = 51;
	r->c.y = 11;
	r->d.x = 12;
	r->d.y = 43;
	obiekty.push_back(r);
	auto t = new Triangle();
	t->a.x = 1;
	t->a.y = 2;
	t->b.x = 3;
	t->b.y = 6;
	t->c.x = 51;
	t->c.y = 11;
	obiekty.push_back(t);
	auto k = new Square();
	k->a.x = 0;
	k->a.y = 0;
	k->b.x = 0;
	k->b.y = 5;
	k->c.x = 5;
	k->c.y = 5;
	k->d.x = 5;
	k->d.y = 0;
	obiekty.push_back(k);
	auto z = new Rhombus();
	z->a.x = 0;
	z->a.y = 0;
	z->b.x = 0;
	z->b.y = 5;
	z->c.x = 5;
	z->c.y = 5;
	z->d.x = 5;
	z->d.y = 0;
	obiekty.push_back(z);
}


