#include "stdafx.h"
#include <string>
#include <iostream>
#include <cmath>
#include "geom.h"
using namespace std;

double Point::calculateDistance(Point & p2)
{
	return  sqrt(pow((this->x - p2.x),2)  + pow((this->y - p2.y), 2));
}

double Rectangle::area()
{
	double p1, p2, pole2;
	p1=b.calculateDistance( a);
	p2=d.calculateDistance(a);
	pole2 = p1*p2;
	return pole2;
}

double Rectangle::size()
{
	Rectangle r = *this;
	return sizeof(r);
}
double Rhombus::size()
{
	Rhombus r = *this;
	return sizeof(r);
}
double Square::size()
{
	Square r = *this;
	return sizeof(r);
}
double Triangle::size()
{
	Triangle r = *this;
	return sizeof(r);
}

double Rectangle::perimiter()
{
	double p1, p2, p3, p4;
	p1=a.calculateDistance(b);
	p2 = d.calculateDistance(a);
	p3 = b.calculateDistance(c);
	p4 = c.calculateDistance(d);
	return  p1 + p2 + p3 + p4;
}
bool Triangle::hasGreaterArea(Polygon* p)
{
	if (this->area() < (p->area()))
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool Rectangle::hasGreaterArea(Polygon* p)
{
	if (this->area() < (p->area()))
	{
		return false;
	}
	else
	{
		return true;
	}
}
double Triangle::perimiter()
{
	double p1, p2, p3;
	p1 = sqrt(((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y)));	//odl ab
	p2 = sqrt(((c.x - a.x)*(c.x - a.x) + (c.y - a.y)*(c.y - a.y)));//odl ac
	p3 = sqrt(((b.x - c.x)*(b.x - c.x) + (b.y - c.y)*(b.y - c.y)));	//old bc
																
	return p1 + p2 + p3;
}

double Triangle::area()		//pole
{
	double pole;
	pole = (1. / 2)*abs((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x));
	return pole;
}

double Rhombus::perimiter()
{
	double p1, p2, p3, p4;
	p1 = a.calculateDistance(b);

	return  4*p1;
}

double Rhombus::area()
{
	double p1, p2;
	p1 = a.calculateDistance(c);
	p2= b.calculateDistance(d);

	return (p1*p2)/2;
}

bool Rhombus::hasGreaterArea(Polygon * obj)
{
	if (this->area() < (obj->area()))
	{
		return false;
	}
	else
	{
		return true;
	}
}
