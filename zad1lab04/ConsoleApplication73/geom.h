//#pragma once

class Point {
public:
	double x, y;
	double calculateDistance(Point &p2);
};

 class Polygon
{
public:
	virtual double perimiter()=0;
	virtual double area()=0;
	virtual double size() = 0;
	virtual bool hasGreaterArea(Polygon *obj) =0;
};
 class Rectangle;
 class Triangle :public virtual Polygon

 {
	 friend class Rectangle;
 public:
	 Point a, b, c;
	 double perimiter();
	 double area();
	 double size();
	 bool hasGreaterArea(Polygon *obj);
 };

 class Rectangle :public virtual Polygon
 {

 public:
	 Point a, b, c, d;
	 double perimiter();
	 double area();
	 double size();
	 bool hasGreaterArea(Polygon *obj);
 };
 class Rhombus :public virtual Polygon
 {
 public:
	 Point a, b, c, d;
	 double perimiter();
	 double area();
	 double size();
	 bool hasGreaterArea(Polygon *obj);
 };

class Square  : Rhombus, public Rectangle
{
public:
	using Rectangle::a;
	using Rectangle::b;
	using Rectangle::c;
	using Rectangle::d;
	double perimiter() { return Rectangle::perimiter(); }
	double area() { return Rectangle::area(); }
	double size();
	bool hasGreaterArea(Polygon *obj) { return Rectangle::hasGreaterArea(obj); };
};








