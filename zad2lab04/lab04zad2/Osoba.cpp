#include "stdafx.h"
#include "Osoba.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <istream>
#include <algorithm>
using namespace std;

vector<Student*>*  CsvLoader::WczytajStudentow(string sciezka)
{
	vector<Student*>* studenci = new vector<Student*>();
	std::ifstream in(sciezka);
	std::string line;
	while (getline(in, line) && in.good())
	{
		std::vector<std::string> row = csv_read_row(line);
		Student* stud =  new Student();
		stud->id = std::stoi(row[0]);
		stud->imie = row[1];
		stud->nazwisko = row[2];
		stud->nr_konta = row[3];
		stud->rokStudiow = std::stoi(row[4]);
		(studenci)->push_back(stud);
	}
	in.close();

	return studenci;
}

void CsvLoader::WczytajOcenyStudentow(vector<Student*>* studenci, string sciezka)
{
	std::ifstream in(sciezka);
	std::string line;

	while (getline(in, line) && in.good())
	{
		std::vector<std::string> row = csv_read_row(line);
		for (int j = 0; j < studenci->size(); j++)
		{
			Student * stud=studenci->at(j);
			if ((stud->id == std::stoi(row[0])))
			{
			std:map<string,double> *oceny = &stud->ocena;
				oceny->insert(std::pair<std::string, double>(row[1], std::stod(row[2])));

			}
		}

	}
	in.close();
}

vector<Pracownik*>* CsvLoader::WczytajPracownikow(string sciezka)
{
	vector<Pracownik*>* pracownicy = new vector<Pracownik*>();
	std::ifstream in(sciezka);
	std::string line;

	while (getline(in, line) && in.good())
	{
		std::vector<std::string> row = csv_read_row(line);

		Pracownik*wsk;

		if (row.size()<6|| row[5].size()<=1 )
		{
			wsk = new NieNauczyciel();
		}
		else
		{
			NauczycielAkad *n = new NauczycielAkad();
			n->stopien = row[5];
			wsk = n;
		}
		wsk->id = std::stoi(row[0]);
		wsk->imie = row[1];
		wsk->nazwisko = row[2];
		wsk->nr_konta = row[3];
		wsk->stanowisko = row[4];
		pracownicy->push_back(wsk);
		NauczycielAkad* naucz = static_cast<NauczycielAkad*>(wsk);
	}

	in.close();

	return pracownicy;
}

void CsvLoader::WczytajWyplatyPracownikow(vector<Pracownik*>* pracownicy, string sciezka)
{
	std::ifstream in(sciezka);
	std::string line;

	while (getline(in, line) && in.good())
	{
		std::vector<std::string> row = csv_read_row(line);
		for (int j = 0; j < pracownicy->size(); j++)
		{
			Pracownik * stud = pracownicy->at(j);
			if (stud->stanowisko == row[0])
			{
				stud->wyplata = std::stod(row[1]);
			}
		}

	}
	in.close();
}

std::vector<std::string> CsvLoader::csv_read_row(std::string &line)
{
	std::stringstream ss(line);
	return csv_read_row(ss);
}

void CsvLoader::ZapiszPrzelewy(vector<Osoba*>* doWyplaty, string sciezka)
{
	string line;
	ofstream  myfile(sciezka);
	if (myfile.is_open())
	{
		for (int i = 0; i < doWyplaty->size(); i++)
		{
			Osoba* s = doWyplaty->at(i);
			myfile << s->nr_konta << delimiter << s->tytulPrzelewu() << delimiter << s->naleznoscOdUczelni() << endl;
		}
		myfile.close();
	}



}

std::vector<std::string> CsvLoader::csv_read_row(std::istream &in)
{
	std::stringstream ss;
	bool inquotes = false;
	std::vector<std::string> row;//relying on RVO
	while (in.good())
	{
		char c = in.get();
		if (!inquotes && c == '"') //beginquotechar
		{
			inquotes = true;
		}
		else if (inquotes && c == '"') //quotechar
		{
			if (in.peek() == '"')//2 consecutive quotes resolve to 1
			{
				ss << (char)in.get();
			}
			else //endquotechar
			{
				inquotes = false;
			}
		}
		else if (!inquotes && c == delimiter) //end of field
		{
			row.push_back(ss.str());
			ss.str("");
		}
		else if (!inquotes && (c == '\r' || c == '\n'))
		{
			if (in.peek() == '\n') { in.get(); }
			row.push_back(ss.str());
			return row;
		}
		else
		{
			ss << c;
		}
	}
	if(ss.str().size()>1)
	row.push_back(ss.str());
	return row;
}

double Student::SredniaOcen()
{
	double srednia = 0;
	typedef map<string, double>::const_iterator MapIterator;
	for (MapIterator iter = ocena.begin(); iter != ocena.end(); iter++)
	{
		srednia += iter->second;
	}
	return srednia / ocena.size();
}

double Student::naleznoscOdUczelni()
{
	return 500;
}

string Student::tytulPrzelewu()
{
	std::stringstream tempString;
	tempString << "Stypendium za wyniki w nauce; " << this->imie << " "<<this->nazwisko;
	return tempString.str();
}

vector<Student*>* Student::StudenciZeStypendium(vector<Student*>* studenci)
{
	vector<Student*>* zeStypendium = new vector<Student*>();
	std::sort(studenci->begin(), studenci->end(), SortujPoSredniej);
	
	for (int i=0;i<(int)studenci->size()/10;i++)
	{
		zeStypendium->push_back(studenci->at(i));
	}
	return zeStypendium;
}

bool Student::SortujPoSredniej(Student* i, Student* j) {
	return i->SredniaOcen() > j->SredniaOcen();
}

string NauczycielAkad::tytulPrzelewu()
{
	std::stringstream tempString;
	tempString << "Wynagrodzenie; " << this->stopien << this->imie<<" " << this->nazwisko;
	return tempString.str();

}

string NieNauczyciel::tytulPrzelewu()
{
	std::stringstream tempString;
	tempString << "Wynagrodzenie; " << this->imie << " " <<this->nazwisko;
	return tempString.str();
}
