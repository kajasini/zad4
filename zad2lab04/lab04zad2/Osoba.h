
#include <string.h>
#include <map>
#include <vector>
using namespace std;
class Osoba
{
public:
	 int id;
	 string imie;
	 string nazwisko;
	 string nr_konta;

	virtual double naleznoscOdUczelni()=0;
	virtual string tytulPrzelewu() = 0;


};

class Student:public  Osoba
{
public:
	int rokStudiow;
	map<string, double>  ocena;
	double SredniaOcen();
	double naleznoscOdUczelni();
	string tytulPrzelewu();
	static vector<Student*>* StudenciZeStypendium(vector<Student*>* studenci);
	static bool SortujPoSredniej(Student* i, Student* j);
};



class Pracownik :public  Osoba
{
public:
	string stanowisko;
	double wyplata;
	double naleznoscOdUczelni() {
		return wyplata;
	}
	virtual string tytulPrzelewu()=0;
};

class NauczycielAkad : public Pracownik
{
public:

	string stopien;
	string tytulPrzelewu();
};

class NieNauczyciel : public Pracownik
{
public:
	string tytulPrzelewu();
	
};
class CsvLoader
{
public:
	static const char delimiter = '|';

	static vector<Student*>* WczytajStudentow(string sciezka);
	static void WczytajOcenyStudentow(vector<Student*>* studenci,string sciezka);
	static vector<Pracownik*>* WczytajPracownikow(string sciezka);
	static void WczytajWyplatyPracownikow(vector<Pracownik*>* pracownicy, string sciezka);
	static std::vector<std::string> csv_read_row(std::istream &in);
	static std::vector<std::string> csv_read_row(std::string &in);
	static void ZapiszPrzelewy(vector<Osoba*>* doWyplaty,string  sciezka);
};