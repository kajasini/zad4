// lab04zad2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Osoba.h"
#include <algorithm>
#ifdef _WIN32
#  include <direct.h>
#  define getcwd _getcwd
#  define chdir _chrdir
#else
#  include <unistd.h>
#include "lab04zad2.h"
#endif
using namespace std;

bool SortujPoWyplacie(Osoba* i, Osoba* j) {
	return i->naleznoscOdUczelni() < j->naleznoscOdUczelni();
}
int main()
{

	vector<Student*>*listaStudentow = CsvLoader::WczytajStudentow("students.csv");

	CsvLoader::WczytajOcenyStudentow(listaStudentow, "grades.csv");
	for (int i = 0; i < listaStudentow->size(); i++)
	{
		Student* s = listaStudentow->at(i);
		cout << s->id << "  " << s->imie << "  " << s->nazwisko << "  " << s->nr_konta <<"	rok studiów:"<<s->rokStudiow<<endl;
		
	std:map<string, double> oceny = s->ocena;
		typedef map<string,double>::const_iterator MapIterator;
		for (MapIterator iter = oceny.begin(); iter != oceny.end(); iter++)
		{
			cout << "Przedmiot: " << iter->first   << "  Ocena: " << iter->second<<endl;
		}
		cout << " srednia wynosi " << s->SredniaOcen();
		cout << endl;
	}


	cout << "ze stypendium " << endl;

	vector<Student*>*zeStypendium= Student::StudenciZeStypendium(listaStudentow);
	for (int i = 0; i < zeStypendium->size(); i++)
	{
		Student* s = zeStypendium->at(i);
		cout << s->id << "  " << s->imie << "  " << s->nazwisko << "  " << s->nr_konta << endl;
		cout << " srednia wynosi " << s->SredniaOcen();
		cout << endl;
	}


	auto listaPracownikow = CsvLoader::WczytajPracownikow("employees.csv");
	CsvLoader::WczytajWyplatyPracownikow(listaPracownikow,"salaries.csv");
	
	
	for (int i = 0; i < listaPracownikow->size(); i++)
	{
		Pracownik* s = listaPracownikow->at(i);
		cout << s->id << "  " << s->imie << "  " << s->nazwisko << "  " << s->nr_konta << "  " << s->stanowisko<<" Wyplata: "<<s->wyplata;
		NauczycielAkad* naucz = dynamic_cast<NauczycielAkad*>(s);
		if (naucz != NULL)
		{
			cout << "  " << naucz->stopien;
		}
		cout << "  " << endl;
	}

	vector<Osoba*>* doWyplaty = new vector<Osoba*>();

		for (int l = 0; l < listaPracownikow->size(); l++)
		{
			Osoba* p = listaPracownikow->at(l);
			doWyplaty->push_back(p);
		}
		for (int l = 0; l < zeStypendium->size(); l++)
		{
			Osoba* p = zeStypendium->at(l);
			doWyplaty->push_back(p);
		}
		std::sort(doWyplaty->begin(), doWyplaty->end(), SortujPoWyplacie);
		cout << "Przelewy: " << endl;

		for (int i = 0; i < doWyplaty->size(); i++)
		{
		Osoba* s = doWyplaty->at(i);
			cout <<  s->nr_konta <<" | " <<s->tytulPrzelewu()<<" | "<<s->naleznoscOdUczelni()<<endl;
		}

		CsvLoader::ZapiszPrzelewy(doWyplaty, "transactions.csv");


	getchar();
	return 0;
}

